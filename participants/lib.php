<?php

function get_participants($startdate, $enddate)
{
	global $DB;

	$sql = 'SELECT ue.id,ue.timestart, u.username, c.fullname, e.enrol FROM {user_enrolments} ue
			JOIN {user} u on ue.userid=u.id
			JOIN {enrol} e on e.id=ue.enrolid 
			JOIN {course} c on c.id=e.courseid
			WHERE ue.timestart between :startdate and :enddate';

	$enddate = date("Y-m-d 23:59:59", $enddate );
	$enddate = strtotime($enddate);
	$result = $DB->get_records_sql($sql, array('startdate' => $startdate, 'enddate' =>$enddate));

	if(empty($result)){
		return false;
	}

	return $result;

}