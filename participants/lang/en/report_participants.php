<?php

$string['participants:view'] = 'View total participants per course report';
$string['pluginname'] = 'Participants Report';
$string['start'] = 'Start Date';
$string['end'] = 'End Date';
$string['timenotvalid'] = "Start date must be greater than End data";
