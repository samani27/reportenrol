<?php

require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once(dirname(__FILE__) . '/report_form.php');
require_once(dirname(__FILE__) . '/lib.php');

require_login();
$context = context_system::instance();
require_capability('report/participants:view', $context);
$PAGE->set_context($context);
$mform = new report_participants_form();

admin_externalpage_setup('report_participants');
echo $OUTPUT->header();

echo html_writer::tag('h1',get_string('pluginname','report_participants'));
echo html_writer::empty_tag('br');

$mform->display();

$data = $mform->get_data();


if($data){
	echo html_writer::empty_tag('br');
	echo html_writer::empty_tag('br');
	echo html_writer::empty_tag('br');
	
	$result = get_participants($data->startdate, $data->enddate);
	
	if($result){
		$table = '';
		$table .= html_writer::tag("th", "#");
		$table .= html_writer::tag("th", "Username");
		$table .= html_writer::tag("th", "Course");
		$table .= html_writer::tag("th", "Time");
		$table .= html_writer::tag("th", "Enrol");
		$table = html_writer::tag("tr", $table);
		$i = 0;
		foreach ($result as $key => $value) {
			$row = '';
			$row .= html_writer::tag("td", ++$i);
			$row .= html_writer::tag("td", $value->username);
			$row .= html_writer::tag("td", $value->fullname);
			$row .= html_writer::tag("td", date("Y-m-d",$value->timestart));
			$row .= html_writer::tag("td", $value->enrol);
			$row = html_writer::tag("tr", $row);
			$table .= $row;
			
			
		}

		echo html_writer::tag("table", $table, array("class" => "table table-bordered table-stripped table-hover"));
		
	} else {
		echo html_writer::tag('p',"Data not found");
	}

} 



echo $OUTPUT->footer();
