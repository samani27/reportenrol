<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');
}

require_once($CFG->libdir . '/formslib.php');

class report_participants_form extends moodleform {

    public function definition() {
        global $CFG, $DB;
        $mform =& $this->_form;
        
        $mform->addElement('date_selector', 'startdate', get_string('start', 'report_participants'));
        $mform->addElement('date_selector', 'enddate', get_string('end', 'report_participants'));


        $this->add_action_buttons(true, "Search");
    }

    public function validation($data, $files) {
        global $DB;
        $errors = parent::validation($data, $files);
        if(!empty($data['startdate']) && !empty($data['enddate'])){

            if($data['startdate'] > $data['enddate']){
                $errors['startdate'] = get_string("timenotvalid", 'report_participants');
            }
        }

        return $errors;
    }
}
