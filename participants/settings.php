<?php

$settings = null;

$ADMIN->add('reports', new admin_externalpage('report_participants',
        get_string('pluginname', 'report_participants'),
        new moodle_url('/report/participants/index.php'),
        'report/participants:view'));
